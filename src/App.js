import React from "react";
import CardView from "./components/cardView";
import TabView from "./components/tabView";
import Container from "@material-ui/core/Container";

function App() {
  return (
    <div className="App">
      <Container>
        <CardView />
        <TabView />
      </Container>
    </div>
  );
}

export default App;
