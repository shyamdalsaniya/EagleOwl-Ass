import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  CardContent,
  Typography,
  Grid,
  CardHeader,
  Box,
  CircularProgress,
  Divider,
} from "@material-ui/core";
import { ArrowUpward, ArrowDownward } from "@material-ui/icons";
import { callAction } from "./action";

const useStyles = makeStyles((theme) => ({
  card: {
    textAlign: "center",
  },
  boxTitle: {
    fontSize: 12,
  },
  arrowSize: {
    fontSize: 14,
  },
  pos: {
    marginTop: 12,
  },
  custom: {
    margin: "25px 0",
  },
  redColor: {
    color: "red",
  },
  greenColor: {
    color: "green",
  },
  devider: {
    margin: "10px 0",
  },
}));

export default function CardView() {
  const classes = useStyles();

  const [highMargin, setHighMargin] = React.useState([]);
  const [lowMargin, setLowMargin] = React.useState([]);
  const [topInfluctuating, setTopInfluctuating] = React.useState([]);

  React.useEffect(() => {
    callAction(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group?order=top"
    ).then((data) => {
      if (data?.results) {
        console.log(data.results);
        setHighMargin(data.results);
      }
    });
    callAction(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group?order=bottom"
    ).then((data) => {
      if (data?.results) {
        console.log(data.results);
        setLowMargin(data.results);
      }
    });
    callAction(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/fluctuation-group?order=top"
    ).then((data) => {
      if (data?.results) {
        console.log(data.results);
        setTopInfluctuating(data.results);
      }
    });
  }, []);

  const RenderProgress = (prop) => {
    return (
      <Box className={classes.boxTitle}>
        {prop.name}
        <Box className={classes.pos} position="relative" display="inline-flex">
          <CircularProgress
            size={70}
            className={prop.margin > 50 ? classes.greenColor : classes.redColor}
            variant="determinate"
            value={prop.margin}
          />
          <Box
            top={0}
            left={0}
            bottom={0}
            right={0}
            position="absolute"
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Typography
              variant="caption"
              component="div"
              className={
                prop.margin > 50 ? classes.greenColor : classes.redColor
              }
            >{`${parseFloat(prop.margin).toFixed(2)}%`}</Typography>
          </Box>
        </Box>
      </Box>
    );
  };

  return (
    <div className={classes.root + " " + classes.custom}>
      <Grid container spacing={3}>
        <Grid item xs={4} sm={4}>
          <Card className={classes.card}>
            <CardContent>
              <CardHeader title="High Margin Recipes" />
              <Box display="flex" justifyContent="center" flexDirection="row">
                {(highMargin || []).map((margin) => {
                  return <RenderProgress {...margin} />;
                })}
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4} sm={4}>
          <Card className={classes.card}>
            <CardContent>
              <CardHeader title="Low Margin Recipes" />
              <Box display="flex" justifyContent="center" flexDirection="row">
                {(lowMargin || []).map((margin) => {
                  return <RenderProgress {...margin} />;
                })}
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4} sm={4}>
          <Card className={classes.card}>
            <CardContent>
              <CardHeader title="Top Fluctualting Recipes" />
              <Box display="flex" justifyContent="center" flexDirection="row">
                {(topInfluctuating || []).map((obj) => {
                  return (
                    <Box
                      className={classes.boxTitle}
                      style={{ padding: "0 5px" }}
                    >
                      {obj.name}
                      <Divider className={classes.devider} />
                      <span
                        className={
                          obj.fluctuation > 50
                            ? classes.greenColor
                            : classes.redColor
                        }
                      >
                        {`${obj.fluctuation}%`}
                        {obj.fluctuation > 50 ? (
                          <ArrowUpward className={classes.arrowSize} />
                        ) : (
                          <ArrowDownward className={classes.arrowSize} />
                        )}
                      </span>
                    </Box>
                  );
                })}
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}
