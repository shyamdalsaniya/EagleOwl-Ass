export async function callAction(url = "", data = {}) {
  const response = await window.fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response.json();
}
