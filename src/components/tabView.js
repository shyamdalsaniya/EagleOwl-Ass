import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  Box,
  Typography,
  Tab,
  Tabs,
  AppBar,
  Chip,
  Paper,
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  TableCell,
  Checkbox,
  TableRow,
  TableHead,
  TableSortLabel,
} from "@material-ui/core";
import { callAction } from "./action";

// styles
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function EnhancedTableHead(props) {
  const {
    classes,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  const headCells = [
    {
      id: "name",
      numeric: false,
      label: "NAME",
    },
    {
      id: "last_updated",
      disablePadding: false,
      label: "LAST UPDATED",
    },
    { id: "cogs", numeric: true, disablePadding: false, label: "COGS" },
    {
      id: "cost_price",
      numeric: true,
      disablePadding: false,
      label: "COST PRICE",
    },
    {
      id: "sale_price",
      numeric: true,
      disablePadding: false,
      label: "SALE PRICE",
    },
    {
      id: "gross_margin",
      disablePadding: false,
      label: "GROSS MARGIN",
    },
    {
      id: "tag",
      numeric: true,
      disablePadding: false,
      label: "TAGS/ACTION",
    },
  ];

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ "aria-label": "select all desserts" }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

function RenderTable({ nextPageUrl, allRecipe, previousPageUrl, setPageUrl }) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("name");
  const [selected, setSelected] = React.useState([]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };
  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = allRecipe.results.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  function stableSort(array, comparator) {
    const stabilizedThis = array?.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }
  function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }
  function getComparator(order, orderBy) {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }
  const isSelected = (name) => selected.indexOf(name) !== -1;

  const handleChangePage = (event, newPage) => {
    let tempPage;
    setPage((p) => {
      tempPage = p;
      return newPage;
    });
    if (nextPageUrl && tempPage < newPage) setPageUrl(nextPageUrl);
    if (previousPageUrl && tempPage > newPage) setPageUrl(previousPageUrl);
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size="small"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={allRecipe?.results?.length}
            />
            <TableBody>
              {stableSort(
                allRecipe?.results ? allRecipe.results : [],
                getComparator(order, orderBy)
              ).map((row, index) => {
                const isItemSelected = isSelected(row.name);
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <TableRow
                    hover
                    onClick={(event) => handleClick(event, row.name)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row.name}
                    selected={isItemSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isItemSelected}
                        inputProps={{ "aria-labelledby": labelId }}
                      />
                    </TableCell>
                    <TableCell
                      component="th"
                      id={labelId}
                      scope="row"
                      padding="none"
                    >
                      {row.name}
                    </TableCell>
                    <TableCell>{row.last_updated.date}</TableCell>
                    <TableCell align="right">{row.cogs}</TableCell>
                    <TableCell align="right">
                      {parseFloat(row.cost_price).toFixed(2)}
                    </TableCell>
                    <TableCell align="right">
                      {parseFloat(row.sale_price).toFixed(2)}
                    </TableCell>
                    <TableCell align="right">
                      {parseFloat(row.gross_margin).toFixed(2)}
                    </TableCell>
                    <TableCell>
                      <Chip label="Tags" variant="outlined" />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          rowsPerPage="10"
          rowsPerPageOptions={[10]}
          count={allRecipe?.count}
          page={page}
          onChangePage={handleChangePage}
        />
      </Paper>
    </div>
  );
}

export default function TabView() {
  const classes = useStyles();
  // use states
  const [value, setValue] = React.useState(0);
  const [allRecipe, setAllRecipe] = React.useState([]);
  const [pageUrl, setPageUrl] = React.useState(
    "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes?page=1"
  );
  const [nextPageUrl, setNextPageUrl] = React.useState("");
  const [previousPageUrl, setPreviousPageUrl] = React.useState("");

  //handle change for change tab
  const handleChange = (event, newValue) => {
    if (newValue === 0) {
      setPageUrl(
        "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes?page=1"
      );
    } else if (newValue === 1) {
      setPageUrl(
        "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes?page=1&is_incorrect=true"
      );
    } else if (newValue === 2) {
      setPageUrl(
        "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes?page=1&is_untagged=true"
      );
    } else if (newValue === 3) {
      setPageUrl(
        "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes?page=1&id_disabled=true"
      );
    }
    setValue(newValue);
  };

  //use effect for call api
  React.useEffect(() => {
    callAction(pageUrl).then((data) => {
      if (data) {
        console.log(data);
        setAllRecipe(data);
        if (data?.next) {
          //the upcomming url from data.next is not valid url so i can not call that api
          setNextPageUrl(`https://beta.eagleowl.in/${data.next}`);
        }
        if (data?.previous) {
          setPreviousPageUrl(`https://beta.eagleowl.in/${data.previous}`);
        }
      }
    });
  }, [pageUrl]);

  return (
    <div className={classes.root}>
      <AppBar position="static" color="white">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs"
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="ALL RECIPES" {...a11yProps(0)} />
          <Tab label="INCORRECT" {...a11yProps(1)} />
          <Tab label="UNTAGGED" {...a11yProps(2)} />
          <Tab label="DISABLED" {...a11yProps(3)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <RenderTable
          nextPageUrl={nextPageUrl}
          allRecipe={allRecipe}
          previousPageUrl={previousPageUrl}
          setPageUrl={setPageUrl}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <RenderTable
          nextPageUrl={nextPageUrl}
          allRecipe={allRecipe}
          previousPageUrl={previousPageUrl}
          setPageUrl={setPageUrl}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <RenderTable
          nextPageUrl={nextPageUrl}
          allRecipe={allRecipe}
          previousPageUrl={previousPageUrl}
          setPageUrl={setPageUrl}
        />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <RenderTable
          nextPageUrl={nextPageUrl}
          allRecipe={allRecipe}
          previousPageUrl={previousPageUrl}
          setPageUrl={previousPageUrl}
        />
      </TabPanel>
    </div>
  );
}
